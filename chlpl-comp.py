import argparse

import lexer, parser, irGenerator
from backends import pyscript, irInterpreter
import debug


# To add a new backend, include it in the `from backends import...` line and add it here
availableBackends = {
    'pyscript': pyscript,
    'irinterpreter': irInterpreter
}
availableBackendNames = tuple(availableBackends.keys())


def getArgs():
    parser = argparse.ArgumentParser('chlpl-comp', description='CHLPL compiler')
    parser.add_argument(
        'infile',
        type=str,
        help='CHLPL source file'
    )
    parser.add_argument(
        'outfile',
        type=str,
        help='Output file'
    )
    parser.add_argument(
        '-r',
        '--reports',
        action='store_true',
        help='Generate reports'
    )
    parser.add_argument(
        '-b',
        '--backend',
        default=availableBackendNames[0],
        const=availableBackendNames[0],
        nargs='?',
        choices=availableBackendNames,
        help='Backend to use (default %(default)s)'
    )
    return parser.parse_args()

def main():
    args = getArgs()

    fileExtensionBase = ''
    try:
        if args.infile[-6:] != '.chlpl':
            fileExtensionBase = '.chlpl'
    except IndexError:
        fileExtensionBase = '.chlpl'

    # Report files
    class _reportData: pass
    reportData = _reportData()

    reportData.generate = args.reports
    reportData.token = args.infile + fileExtensionBase + '-tk'
    reportData.strippedToken = args.infile + fileExtensionBase + '-stk'
    reportData.ast = args.infile + fileExtensionBase + '-ast'
    reportData.ir = args.infile + fileExtensionBase + '-ir'
    reportData.dbg = args.infile + fileExtensionBase + '-dbg'

    # Ensure that old report files are clear to avoid confusion
    with open(reportData.token, 'w') as f: f.close()
    with open(reportData.strippedToken, 'w') as f: f.close()
    with open(reportData.ast, 'w') as f: f.close()
    with open(reportData.ir, 'w') as f: f.close()
    with open(reportData.dbg, 'w') as f: f.close()

    # Set debug defaults
    debug.defaults.msgFile = reportData.dbg
    debug.defaults.printMsg = False
    
    dbg = debug.debugHandler('chlpl-comp')

    dbg.debug('Loading source file {}'.format(args.infile))
    with open(args.infile, 'r') as sourcefile:
        source = sourcefile.read()

    backend = availableBackends[args.backend]

    stream = lexer.lex(source, reportData)
    stream = lexer.strip(stream, reportData)
    ast = parser.parseStream(stream, reportData)
    ir = irGenerator.generate(ast, reportData)
    # Eventually there will be layers of processing here
    backend.process(ir, reportData, args.outfile)

if __name__ == '__main__':
    main()