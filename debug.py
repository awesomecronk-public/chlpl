class _defaults: pass
defaults = _defaults()
defaults.msgFile = None
defaults.printMsg = True

debugHandlers = {}
def debugHandler(name):
    if not name in debugHandlers.keys():
        debugHandlers[name] = _debugHandler(name, defaults.msgFile, defaults.printMsg)
    return debugHandlers[name]


class _debugHandler:
    def __init__(self, name, msgFile, printMsg):
        self.name = name
        self.msgFile = msgFile
        self.printMsg = printMsg

    def format(self, level, msg):
        return '{} ({}): {}\n'.format(level, self.name, msg)

    def message(self, level, msg, printMsg=None):
        if printMsg or ((printMsg is None) and self.printMsg):
            print(self.format(level, msg), end='')

        if self.msgFile != None:
            with open(self.msgFile, 'a') as file:
                file.write(self.format(level, msg))

    def debug(self, msg):
        self.message('DEBUG', msg)

    def info(self, msg):
        self.message('INFO', msg)

    def warning(self, msg):
        self.message('WARNING', msg, printMsg=True)

    def error(self, msg):
        self.message('ERROR', msg, printMsg=True)
