# CHLPL: Cronk's High-Level Programming Language

(pronounced "chill pill")

CHLPL is a dynamic-typed, high-level programming language designed to be easy on the eyes and easier on the brain.
