import sys

from debug import debugHandler


# Token types:
# - SPACE           - boring whitespace, not usually doing much
# - NEWLINE         - a newline character
# - INDENT          - interesting whitespace, determining when a section of code ends
# - STRING          - text contained within quotes
# - INT             - numbers, valid chars 0123456789ABCDEFdDbBoOxX
# - KEYWORD         - if, else, while, break, nobreak, etc.
# - DIRECTIVE       - things starting with @
# - NAME            - anything that's a valid name and not a keyword
# - STORE           - =
# - OPERATOR        - +, -, *, ^, /, etc.
# - INPLACEOPERATOR - +=, -=, *=, ^=, /=, etc.
# - COMPARATOR      - >, <, >=, <=, ==, etc.
# - (L/R)PAREN      - ( and )
# - (L/R)BRACKET    - [ and ]
# - (L/R)BRACE      - { and }


commentBeginChar = '#'

keywords = [
    'if', 'elsif', 'else',
    'loop', 'times', 'for', 'in', 'while',
    'break', 'broken', 'unbroken',
    'function', 'class', 'return',
    'as'
]
nameValidBeginChars = 'abcedfghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_'
nameValidChars = 'abcedfghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_'

directiveBeginChar = '@'
directiveValidChars = '@abcedfghijklmnopqrstuvwxyz'

floatValidBeginChars = '0123456789'
floatValidChars = '0123456789.'
floatDecimalChar = '.'

intValidBeginChars = '0123456789'
intValidChars = '0123456789abcdefABCDEFdDbBoOxX'

newlineChar = '\n'
spaceChar = ' '
indentSize = 4

strTermChars = '\'"'
strEscapeChar = '\\'
strEscapes = {
    'n': '\n',
    '\n': '\n'  # Allows a string to span multiple lines by tacking a \ at the end of each line 
}

lparenChar = '('
rparenChar = ')'
lbracketChar = '['
rbracketChar = ']'
lbraceChar = '{'
rbraceChar = '}'

operators = [
    '+', '-', '*', '/', '**', '%',
    '<', '>', '<=', '>=', '=='
]
inplaceOperators = [
    '+=', '-=', '*=', '/=', '**=', '%='
]
operatorValidChars = '+-*/%<>='

equalsChar = '='
dotChar = '.'
colonChar = ':'
commaChar = ','


class Token:
    validTypes = [
        'SPACE',
        'NEWLINE',
        'INDENT',
        'STRING',
        'INT',
        'KEYWORD',
        'DIRECTIVE',
        'NAME',
        'STORE',
        'OPERATOR',
        'COMPARATOR'
    ]
    allowAnyType = True
    compareText = False

    class tempCompareText():
        # There is no need to keep a stack of values because in the event that another context
        # manager is created it will independently track the value through out its indented block
        def __init__(self, value):
            self.value = value

        def __enter__(self):
            self.originalCompareText = Token.compareText
            Token.compareText = self.value

        def __exit__(self, excType, excValue, excTraceback):
            Token.compareText = self.originalCompareText
    
    def __init__(self, tokenType, line=None, col=None, text=None):
        if tokenType in self.validTypes or self.allowAnyType: self.tokenType = tokenType
        else: raise ValueError('Invalid token type: {}'.format(tokenType))
        
        self.line = line
        self.col = col
        self.text = text

    def __repr__(self):
        return 'token {}{}{}'.format(
            self.tokenType,
            '' if self.text is None else ' ({})'.format(repr(self.text)),
            '' if (self.line is None or self.col is None) else ' (l{}c{})'.format(self.line, self.col)
        )

    def __eq__(self, other):
        if isinstance(other, Token):
            result = self.tokenType == other.tokenType
            if self.compareText:
                result = result and self.text == other.text
            return result
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)


def lex(source, reportData):
    dbg = debugHandler('lexer.lex')
    dbg.debug('Lexing source')

    line = 1
    col = 1
    char = ''
    source = iter(source)
    tokens = []


    def nextChar():
        nonlocal line, col, char

        # Update line/column for next char
        if char == '\n':
            col = 1
            line += 1
        elif char != '':   # Initialization value of char is ''
            col += 1

        char = next(source)
        # print('char {} (l{}c{})'.format(repr(char), line, col))


    def addToken(tokenType, line, col, text):
        tokens.append(Token(tokenType, line, col, text))


    try: nextChar()  # Preload first character
    except StopIteration:
        dbg.info('infile is empty')
        return tokens

    runFlag = True


    while runFlag:
        
        # Each section assumes that char has been iterated since it was last processed and updates
        # it before exiting unless an error occurs, in which case it exits

        # Consume a comment
        if char == commentBeginChar:
            # dbg.info('Consuming comment)
            comment = ''
            while char != newlineChar:
                comment = comment + char
                try: nextChar()
                except StopIteration:
                    runFlag = False
                    break
            addToken('COMMENT', line, col - len(comment), comment)

        # Consume a name or keyword
        elif char in nameValidBeginChars:
            # dbg.info('Consuming name or keyword')
            name = ''
            while char in nameValidChars:
                name = name + char
                try: nextChar()
                except StopIteration:
                    runFlag = False
                    break

            if name in keywords: addToken('KEYWORD', line, col - len(name), name)
            else: addToken('NAME', line, col - len(name), name)

        
        # Consume a directive
        elif char == directiveBeginChar:
            # dbg.info('Consuming directive')
            directive = ''
            while char in directiveValidChars:
                directive = directive + char
                try: nextChar()
                except StopIteration:
                    runFlag = False
                    break

            addToken('DIRECTIVE', line, col - len(directive), directive)


        # Consume a float or int
        # Must be done before dot
        elif char in floatValidBeginChars:
            # dbg.info('Consuming float')
            floatNumber = ''
            while char in floatValidChars:
                floatNumber = floatNumber + char
                try: nextChar()
                except StopIteration:
                    runFlag = False
                    break

            decimalPoints = floatNumber.count(floatDecimalChar)

            if decimalPoints == 0:
                addToken('INT', line, col - len(floatNumber), floatNumber)
            elif decimalPoints == 1:
                addToken('FLOAT', line, col - len(floatNumber), floatNumber)
            else:
                addToken('UNKNOWN', line, col - len(floatNumber), floatNumber)


        # Consume a newline
        elif char == newlineChar:
            # dbg.info('Consuming newline')
            addToken('NEWLINE', line, col, char)
            try: nextChar()
            except StopIteration:
                runFlag = False


        # Consume an indent
        elif char == spaceChar and tokens[-1].tokenType == 'NEWLINE':
            # dbg.info('Consuming indent')
            indent = ''
            while char == spaceChar:
                indent = indent + char
                try: nextChar()
                except StopIteration:
                    runFLag = False
                    break
                if len(indent) == indentSize:
                    addToken('INDENT', line, col - len(indent), indent)
                    indent = ''

            # If there is leftover space that doesn't fill an indent level, tokenize it as a space
            if len(indent):
                addToken('SPACE', line, col - len(indent), indent)


        # Consume a space
        elif char == spaceChar:
            # dbg.info('Consuming space')
            space = ''
            while char == spaceChar:
                space = space + char
                try: nextChar()
                except StopIteration:
                    runFlag = False
                    break

            addToken('SPACE', line, col - len(space), space)


        # Consume a string
        elif char in strTermChars:
            string = ''
            strTermChar = char
            escape = False
            while True:
                try: nextChar()
                except StopIteration:
                    dbg.error('l{}c{}: Unexpected EOF while lexing string'.format(line, col))
                    sys.exit(1)

                if escape:
                    if char in strEscapes.keys():
                        string = string + strEscapes[char]
                    else:
                        dbg.error('l{}c{}: Unrecognized escape char \'\\{}\''.format(line, col, char))
                        sys.exit(1)
                    escape = False

                else:
                    if char == strEscapeChar:
                        escape = True
                    elif char == strTermChar:
                        break
                    else:
                        string = string + char

            # This particular consumer actually consumes the last character it calls for, needs an
            # extra nextChar call to fix that
            try: nextChar()
            except StopIteration:
                runFlag = False
            addToken('STRING', line, col - len(string), string)


        # Consume parens, brackets, or braces
        elif char == lparenChar:
            addToken('LPAREN', line, col, char)
            try: nextChar()
            except StopIteration:
                runFlag = False
        elif char == rparenChar:
            addToken('RPAREN', line, col, char)
            try: nextChar()
            except StopIteration:
                runFlag = False
        elif char == lbracketChar:
            addToken('LBRACKET', line, col, char)
            try: nextChar()
            except StopIteration:
                runFlag = False
        elif char == rbracketChar:
            addToken('RBRACKET', line, col, char)
            try: nextChar()
            except StopIteration:
                runFlag = False
        elif char == lbraceChar:
            addToken('LBRACE', line, col, char)
            try: nextChar()
            except StopIteration:
                runFlag = False
        elif char == rbraceChar:
            addToken('RBRACE', line, col, char)
            try: nextChar()
            except StopIteration:
                runFlag = False


        # Consume an operator or inplace operator
        elif char in operatorValidChars:
            # dbg.info('Consuming operator')
            operator = ''
            while char in operatorValidChars:
                operator = operator + char
                try: nextChar()
                except StopIteration:
                    runFlag = False
                    break

            if operator in operators:
                addToken('OPERATOR', line, col - len(operator), operator)
            elif operator in inplaceOperators:
                addToken('INPLACEOPERATOR', line, col - len(operator), operator)
            elif operator == equalsChar:
                addToken('EQUALS', line, col - len(operator), operator)
            else:
                addToken('UNKNOWN', line, col - len(operator), operator)

        
        # Consume dots, colons, or commas
        elif char == dotChar:
            addToken('DOT', line, col, char)
            try: nextChar()
            except StopIteration:
                runFlag = False
        elif char == colonChar:
            addToken('COLON', line, col, char)
            try: nextChar()
            except StopIteration:
                runFlag = False
        elif char == commaChar:
            addToken('COMMA', line, col, char)
            try: nextChar()
            except StopIteration:
                runFlag = False


        # Consume unknown tokens
        else:
            # dbg.info('Consuming unknown character')
            addToken('UNKNOWN', line, col, char)
            try: nextChar()
            except StopIteration:
                runFlag = False


    if reportData.generate:
        tokenAnalysis = {'UNKNOWN': 0}

        with open(reportData.token, 'w') as reportFile:
            for token in tokens:
                reportFile.write(repr(token) + '\n')

                if token.tokenType in tokenAnalysis.keys():
                    tokenAnalysis[token.tokenType] += 1
                else:
                    tokenAnalysis[token.tokenType] = 1

            for tokenType in tokenAnalysis.keys():
                if tokenType != 'UNKNOWN':
                    tokenCount = tokenAnalysis[tokenType]
                    reportFile.write('{} {} token{}\n'.format(tokenCount, tokenType, 's' if tokenCount > 1 else ''))
        
            tokenCount = tokenAnalysis['UNKNOWN']

            if tokenCount:
                reportFile.write('{} UNKNOWN token{}\n'.format(tokenCount, 's' if tokenCount > 1 else ''))
                unknownTokens = []
                for token in tokens:
                    if token.tokenType == 'UNKNOWN':
                        unknownTokens.append(token.text)
                reportFile.write('UNKNOWN token{} "{}"\n'.format('s are' if tokenCount > 1 else ' is', '", "'.join(unknownTokens)))


    return tokens


def strip(stream, reportData):
    dbg = debugHandler('lexer.strip')
    dbg.debug('Stripping stream')
    # Remove newlines at beginning of source
    while stream[0] == Token('NEWLINE'):
        del stream[0]

    # enumerate was getting mixed up as the stream mutated
    # for i, item in enumerate(stream):
    i = 0
    while i < len(stream):
        item = stream[i]
        # Remove comments
        if item == Token('COMMENT'):
            del stream[i]

        # Remove whitespace
        elif item == Token('SPACE'):
            del stream[i]

        # Remove consecutive newlines
        elif item == Token('NEWLINE'):
            if i > 1 and stream[i - 1] == Token('NEWLINE'):
                del stream[i]
            else:
                i += 1

        # If something was removed we don't want to increment i because the next item is then at the current index
        else: i += 1

    if reportData.generate:
        with open(reportData.strippedToken, 'w') as reportFile:
            for item in stream:
                reportFile.write(repr(item) + '\n')

    return stream
