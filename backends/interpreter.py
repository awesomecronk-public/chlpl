import sys

from lexer import Token
from parser import Node

from debug import debugHandler


class chlplException(Exception): pass

class chlplFunction:
    def __init__(self, code, argNames, isPython=False):
        self.code = code
        self.argNames = argNames
        self.isPython = isPython

class chlplScope:
    nextID = 0
    def __init__(self, parent=None):
        self.parent = parent
        self.objects = {}
        self.id = chlplScope.nextID
        chlplScope.nextID += 1
        self.dbg = debugHandler('chlplScope')

    def __repr__(self):
        return '<chlplScope {}>'.format(self.id)

    # Find an object with the given name and return the scope in which it resides
    def getObjectScope(self, name, checkParent=True):
        if name in self.objects.keys():
            return self
        elif checkParent and self.parent != None:
            return self.parent.findObject(name, checkParent)
        else:
            return None
    
    def getVar(self, name):
        residingScope = self.getObjectScope(name)
        self.dbg.debug('Getting variable {} from scope {}'.format(name, residingScope))
        var = residingScope.objects[name]
        if var is None: dbg.error('No variable by the name {}'.format(name))
        return var

    # Once directives are implemented one will be provided to associate a variable name with the global scope
    def setVar(self, name, value):
        self.dbg.debug('Setting variable {} in {}'.format(name, self))
        self.objects[name] = value


builtinFunctions = {
    'print': chlplFunction(print, ['string'], isPython=True),
    'exit': chlplFunction(sys.exit, ['exitCode'], isPython=True)
}


def execute(ast):
    dbg = debugHandler('interpreter.execute')
    dbg.debug('Begin execution')
    globalScope = chlplScope()
    dbg.debug('Global scope is {}'.format(globalScope))

    for functionName in builtinFunctions.keys():
        globalScope.objects[functionName] = builtinFunctions[functionName]

    exitCode = executeBlock(ast, globalScope)   # AST is provided as the main block

    if exitCode is None: exitCode = 0
    dbg.debug('Program exited with code {}'.format(exitCode))
    sys.exit(exitCode)

def executeBlock(block, scope):
    dbg = debugHandler('interpreter.executeBlock')
    for node in block.components:
        if node == Node('DIRECTIVE'):
            dbg.warning('Not processing directive')

        elif node == Node('STORE'):
            dbg.debug('Processing store')

            value = evaluateExpression(node.components[0], scope)
            scope.setVar(node.parameter, value)

        elif node == Node('CALL'):
            callFunction(node, scope)

        # If there is no RETURN executeBlock will return None by default, courtesy of Python
        elif node == Node('RETURN'):
            if len(node.components):
                value = evaluateExpression(node.components[0], scope)
            else:
                value = None
            return value

        else:
            dbg.error('Unknown node encountered: {}'.format(node.nodeType))
            sys.exit(1)

def evaluateExpression(expr, scope):
    dbg = debugHandler('interpreter.evaluateExpression')
    if expr == Node('EXPRESSION'):
        dbg.debug('Evaluating expression ({})'.format(expr.parameter))
        components = [evaluateExpression(component, scope) for component in expr.components]
        
        if expr.parameter == '+':
            value = components[0] + components[1]

        elif expr.parameter == '-':
            value = components[0] - components[1]

        elif expr.parameter == '*':
            value = components[0] * components[1]

        elif expr.parameter == '/':
            value = components[0] / components[1]

        else:
            dbg.error('Unknown operator {}'.format(expr.parameter))
            sys.exit(1)

    elif expr == Node('FUNCTION'):
        if expr.parameter == Node('COMMALIST'):
            argnames = [item.text for item in expr.parameter.components]
        elif expr.parameter == Token('NAME'):
            argnames = [parameter.text]
        else:
            dbg.error('Can\'t build function with {} for argument names')
        func = chlplFunction(expr.components[0], argnames)
        return func

    elif expr == Node('CALL'):
        return callFunction(expr, scope)

    else:
        dbg.debug('Evaluating expression component:\n{}'.format(expr))
        if expr == Token('NAME'):
            value = scope.getVar(expr.text)

        elif expr == Token('INT'):
            value = int(expr.text)

        else:
            dbg.error('evaluateExpression doesn\'t know how to handle {}'.format(expr))
            sys.exit(1)

    return value

def callFunction(callNode, callerScope):
    dbg = debugHandler('interpreter.callFunction')
    dbg.debug('Processing call')

    function = callerScope.getVar(callNode.parameter)
    arguments = []

    # Get arguments if they exist
    if len(callNode.components):
        if callNode.components[0] == Node('COMMALIST'):
            arguments = [evaluateExpression(item, callerScope) for item in callNode.components[0].components]
        else:
            arguments = [evaluateExpression(callNode.components[0], callerScope)]

    # Check argument count
    if len(arguments) > len(function.argNames):
        dbg.error('Too many arguments to {}'.format(function.name))
    elif len(arguments) < len(function.argNames):
        dbg.error('Too many arguments to {}'.format(function.name))

    # Call the function
    if function.isPython:
        returnValue = function.code(*arguments)
    
    # Build a scope for the function call and execute its code block
    else:
        callScope = chlplScope(parent=callerScope)

        for a, argumentName in enumerate(function.argNames):
            callScope.setVar(argumentName, arguments[a])
        
        returnValue = executeBlock(function.code, scope=callScope)

    # I would just directly return from the execution calls but idk what else I may add between it
    # and callFunction returning
    return returnValue
