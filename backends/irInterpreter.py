import sys
from types import MethodType

from debug import debugHandler

from irGenerator import Block, Instr, Value, Name, Const

# TODO NEXT: Make CALL getValue() of each arg, then pass the values instead


class Namespace:
    def __init__(self, parent=None):
        self.contents = {}

        if not (isinstance(parent, Namespace) or parent is None):
            raise TypeError('Expected Namespace or None for parent, got {}'.format(parent))
        self.parent = parent

    def get(self, name):
        if name in self.contents.keys():
            return self.contents[name]
        
        elif self.parent != None:
            return self.parent.get(name)
        
        else:
            return None

    def set(self, name, value):
        self.contents[name] = value

class Function:
    def __init__(self, block, parentNamespace):
        self.block = block
        self.parentNamespace = parentNamespace
    
    def call(self, args):
        namespace = Namespace(self.parentNamespace)
        return execBlock(self.block, args, namespace)

class PyFunction(Function):
    def __init__(self, parentNamespace):
        Function.__init__(self, None, parentNamespace)

    def call(self, args):
        dbg.error('PyFunction.call not implemented')
        sys.exit(1)
        

dbg = debugHandler('irInterpreter')
dbg.instr = MethodType(lambda self, msg: self.message('INSTR', msg), dbg)


def process(ir, reportdata, outfilePath):
    # Set up default main namespace
    mainNamespace = Namespace()
    
    pyPrint = PyFunction(mainNamespace)
    pyPrint.call = MethodType(lambda self, args: print(*args), pyPrint)
    mainNamespace.set('print', pyPrint)

    execBlock(ir[0], [], mainNamespace)

def execBlock(block, args, namespace):
    dbg.debug('Executing block {}'.format(block.num))

    # Init unnamed values (for storing values created by breaking up complex expressions into TAC)
    unnamedValues = []

    # Check correct number of args
    if len(args) != len(block.args):
        dbg.error('RUNTIME ERR: Mismatch arg count ({}/{})'.format(len(args), len(block.args)))
        sys.exit(1)
    
    # Get arg values
    for a in range(len(args)):
        argName = block.args[a].name
        argVal = args[a]
        namespace.set(argName, argVal)

    def getValue(item):
        dbg.debug('getValue({})'.format(item))
        if isinstance(item, Name): value = (namespace.get(item.name))
        elif isinstance(item, Value): value = (unnamedValues[item.num])
        elif isinstance(item, Const): value = (item.value)
        else: dbg.error('Cannot get value of {}'.format(item)); sys.exit(1)
        dbg.debug('getValue({}) -> {}'.format(item, repr(value)))
        return value

    def setValue(dest, value):
        dbg.debug('setValue({}, {})'.format(dest, value))
        if isinstance(dest, Name): namespace.set(dest.name, value)
        elif isinstance(dest, Value):
            if len(unnamedValues) != dest.num:
                dbg.error('Tried to set value {} but {} values exist'.format(dest.num, len(unnamedValues)))
                sys.exit(1)
            unnamedValues.append(value)
        else: dbg.error('Cannot set value of {}'.format(dest)); sys.exit(1)

    # Execute code
    for instr in block.code:
        dbg.instr("{} ({})".format(instr.op, ', '.join([repr(arg) for arg in instr.args])))

        if instr.op == 'FUNCTION':
            function = Function(instr.args[0], namespace)
            setValue(instr.dest, function)

        elif instr.op == 'CALL':
            function = getValue(instr.args[0])
            args = [getValue(arg) for arg in instr.args[1:]]
            result = function.call(args)
            dbg.debug('Returned from call')
            if instr.dest != None: setValue(instr.dest, result)

        # Not yet sure if returning multiple values will be handled by packing them into an array
        # first or just adding them all as args to RETURN
        elif instr.op == 'RETURN':
            return instr.args[0]

        elif instr.op == 'COPY':
            setValue(instr.dest, getValue(instr.args[0]))

        elif instr.op == 'ADD':
            setValue(instr.dest, getValue(instr.args[0]) + getValue(instr.args[1]))

        elif instr.op == 'SUB':
            setValue(instr.dest, getValue(instr.args[0]) - getValue(instr.args[1]))

        elif instr.op == 'MULT':
            setValue(instr.dest, getValue(instr.args[0]) * getValue(instr.args[1]))

        # In CHLPL int / int returns the floor division of the two as an int, like C, not Python.
        elif instr.op == 'DIV':
            setValue(instr.dest, getValue(instr.args[0]) // getValue(instr.args[1]))

        else:
            dbg.error('Unknown instr.op "{}"'.format(instr.op))
            sys.exit(1)
