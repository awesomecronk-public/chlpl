import os, sys

from debug import debugHandler
from irGenerator import Block, Instr, Name, Value, Const


functions = []


pyNames = ['print']

# These values can be changed at runtime
nextLoopID = 0


def _addIndent(lines, level):
    return  [('    ' * level) + line for line in lines]


# Return the Python representation of an object
def pyRepr(obj):
    if isinstance(obj, Const):
        return obj.value
    
    elif isinstance(obj, Name):
        if obj.name in pyNames:
            return obj.name
        else:
            return obj.tag

    else:
        return obj.tag


def process(ir, reportData, outfilePath):
    dbg = debugHandler('pyscript.process')

    global functions
    functions = ir[1:]
    
    pyCode = '#!/bin/python3\n\n' + '\n'.join(processFunc(ir[0], None))

    dbg.info('Writing Python code to {}'.format(outfilePath))
    with open(outfilePath, 'w') as outfile:
        outfile.write(pyCode)
    
    outfilePerms = 0o755
    dbg.info('Setting {0:3o} permissions'.format(outfilePerms))
    os.chmod(outfilePath, outfilePerms)
    
    dbg.info('Done')
    

def processFunc(func, funcDest):
    global nextLoopID

    dbg = debugHandler('pyscript.processFunction')
    dbg.debug('Processing {}'.format(func.tag))

    lines = []

    for instr in func.code:
        # Treat function and loop instructions differently, need to recurse for them
        if instr.op == 'FUNCTION':
            lines += processFunc(instr.args[0], instr.dest)

        elif instr.op[0:4] == 'LOOP':
            loopID = nextLoopID; nextLoopID += 1
            modifierArgs = instr.args[1:]
            dbg.debug('Loop {} modifier args: {}'.format(loopID, modifierArgs))

            loopHeaderLines = ['# Loop ID {}'.format(loopID), 'while True:']
            loopBlockPrefixLines = ['# Loop prefix']
            loopBlockPostfixLines = ['# Loop postfix']
            loopBlockLines = processFunc(instr.args[0], None)
            
            # Add times modifier
            if 'T' in instr.op:
                loopHeaderLines.insert(-1, 'tCnt{} = 0'.format(loopID))
                loopBlockPrefixLines.append('if tCnt{} >= {}: break'.format(loopID, pyRepr(modifierArgs[0])))
                loopBlockPostfixLines.append('tCnt{} += 1'.format(loopID))
                del modifierArgs[0]

            # Add iterator modifier
            if 'I' in instr.op:
                loopHeaderLines.insert(-1, 'iInd{} = 0'.format(loopID))
                loopBlockPrefixLines.append('if iInd{} >= len({}): break'.format(loopID, pyRepr(modifierArgs[1])))
                loopBlockPrefixLines.append('{} = {}[iInd{}]'.format(pyRepr(modifierArgs[0]), pyRepr(modifierArgs[1]), loopID))
                loopBlockPostfixLines.append('iInd{} += 1'.format(loopID))
                dbg.debug('{}'.format(type(modifierArgs[0])))
                dbg.debug('{}'.format(type(modifierArgs[1])))
                del modifierArgs[0:2]

            # Add condition modifier
            # TODO: Finish
            if 'C' in instr.op:
                dbg.warning('Not processing condition modifier')

            loopBlockLines = loopBlockPrefixLines + [''] + loopBlockLines + loopBlockPostfixLines + ['']
            loopBlockLines = _addIndent(loopBlockLines, 1)

            lines += loopHeaderLines
            lines += loopBlockLines

        # Everything else can have its own line
        else:
            if instr.op == 'ADD':
                op = '{} + {}'.format(pyRepr(instr.args[0]), pyRepr(instr.args[1]))

            elif instr.op == 'DIV':
                op = '{} // {}'.format(pyRepr(instr.args[0]), pyRepr(instr.args[1]))

            elif instr.op == 'CALL':
                op = '{}({})'.format(pyRepr(instr.args[0]), ', '.join([pyRepr(arg) for arg in instr.args[1:]]))

            elif instr.op == 'RETURN':
                op = 'return {}'.format(pyRepr(instr.args[0]))

            else:
                dbg.error('Unknown instr op "{}"'.format(instr.op))
                sys.exit(1)

            if instr.dest is None: line = op
            else: line = '{} = {}'.format(pyRepr(instr.dest), op)
            lines.append(line)

    # A FUNCTION instr should always have a dest of some sort, so dest = None means it must be
    # either the root of the file or an indented block and shouldn't get a def line
    if funcDest is None:
        lines.insert(0, '# {}'.format(pyRepr(func)))
        lines.append('')

    else:
        # Build `def funcName(args):` and insert at 0
        defLine = 'def {}({}):'.format(pyRepr(funcDest), ', '.join([pyRepr(arg) for arg in func.args]))
        indentedLines = ['    ' + line for line in lines]   # Indent the code
        lines = ['', defLine, '    # {}'.format(pyRepr(func))] + indentedLines + ['',]

    return lines
