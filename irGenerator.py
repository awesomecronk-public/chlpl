import sys

from debug import debugHandler
from lexer import Token
from parser import Node


class Block:
    reprCode = True
    nextNum = 0

    def __init__(self, args: list):
        self.args = args
        self.code = []
        self.num = Block.nextNum
        Block.nextNum += 1
        self.tag = 'b_{}'.format(self.num)

    def __repr__(self):
        return 'block {}'.format(self.num)

    def __str__(self):
        text = 'block {}'.format(self.num)
        if self.reprCode:
            if self.args: text = text + '\n    args: ' + ', '.join([repr(arg) for arg in self.args])
            text = text + '\n    code:'
            for instr in self.code:
                text = text + '\n        ' + repr(instr)
        return text

class Instr:
    def __init__(self, op, args=[], dest=None):
        self.op = op
        self.args = args
        self.dest = dest

    def __repr__(self):
        text = 'instr ' + self.op
        if self.args:
            text = text + ' ({})'.format(', '.join(arg.tag for arg in self.args))
        if self.dest != None:
            text = text + ' -> ' + self.dest.tag
        return text

class Value:
    nextNum = 0

    def __init__(self):
        self.num = Value.nextNum
        Value.nextNum += 1
        self.tag = 'v_{}'.format(self.num)

    def __repr__(self):
        return 'value {}'.format(self.num)

class Name:
    def __init__(self, name):
        self.name = name
        self.tag = 'n_' + self.name

    def __repr__(self):
        return 'name "{}"'.format(self.name)

class Const:
    def __init__(self, value):
        self.value = value
        self.tag = 'c_' + str(self.value)

    def __repr__(self):
        return 'const {}'.format(self.value)


ir = []     # [Func, Func, ...]


def generate(ast, reportData):
    global ir; ir = []     # Ensure that ir is empty
    dbg = debugHandler('irGen.generate')

    entryBlock = Block([])
    ir.append(entryBlock)
    processBlock(ast, entryBlock)

    if reportData.generate:
        with open(reportData.ir, 'w') as reportFile:
            for block in ir:
                reportFile.write(repr(block) + '\n')

    return ir


def processBlock(astBlock, irBlock):
    dbg = debugHandler('irGen.processBlock')
    dbg.debug('Processing block for {}'.format(irBlock.tag))

    nextLabel = 0

    # for node in astBlock.components:
    n0 = 0
    while True:
        node = astBlock.components[n0]
        
        if node == Node('DIRECTIVE'):
            dbg.warning('Not processing directive')

        elif node == Node('STORE'):
            dbg.debug('Processing store')
            # Process the expression and put the value in the desired name
            processExpression(node.components[0], irBlock, dest=Name(node.parameter))

        elif node == Node('RETURN'):
            dbg.debug('Processing return')
            # Process the expression and create a value for it
            value = processExpression(node.components[0], irBlock)
            irBlock.code.append(Instr('RETURN', [value]))

        elif node == Node('CALL'):
            processCall(node, irBlock)

        elif node == Node('LOOP'):
            processLoop(node, irBlock)

        elif node == Node('IF'):
            dbg.debug('Processing if')
            dbg.debug('n0 is {}, points at first if'.format(n0))
            elsifNodes = []
            n1 = 1
            while astBlock.components[n0 + n1] == Node('ELSIF'):
                elsifNodes.append(astBlock.components[n0 + n1])
                n1 += 1
                if (n0 + n1) >= len(astBlock.components): break

            dbg.debug('Found {} elsif nodes'.format(len(elsifNodes)))
            
            # n1 points to the node after the last ELSIF, which would be the ELSE if there is one
            if astBlock.components[n0 + n1] == Node('ELSE'):
                elseNode = astBlock.components[n0 + n1]
                n1 += 1
            
                dbg.debug('Found an else node')
            
            else: elseNode = None

            n0 += (n1 - 1)  # n1 points to next node, but that doesn't work since we'll increment n0 at the end of the loop
            dbg.debug('n0 is {}, points at first if or last elsif/else'.format(n0))

        elif node == Node('ELSIF'):
            dbg.debug('(l{}c{}) elsif with no if'.format(node.line, node.col))
            sys.exit(1)

        elif node == Node('ELSE'):
            dbg.debug('(l{}c{}) else with no if'.format(node.line, node.col))
            sys.exit()

        else:
            dbg.error('Don\'t know what to do with this: {}'.format(node))
            sys.exit(1)

        n0 += 1
        if n0 >= len(astBlock.components): break

    dbg.debug('Done processing block for {}'.format(irBlock.tag))

def processExpression(expr, block, dest=None):
    # In the event of an expression, an instruction needs to be executed to create the value
    # desired by whatever called to process the expression. That value is what gets returned from
    # processExpression. In cases where an instruction does not need created (Like
    # processExpression called recursively for each component of the expression. The last calls
    # will always be on single components, which can simply be returned.), the value can simply be
    # resolved and returned.

    dbg = debugHandler('irGen.processExpression')

    if expr == Node('EXPRESSION'):
        dbg.debug('Processing expression')
        a = processExpression(expr.components[0], block)
        b = processExpression(expr.components[1], block)
        
        try:
            op = {
                '+': 'ADD',
                '-': 'SUB',
                '*': 'MULT',
                '/': 'DIV'
            }[expr.parameter]
        except ValueError:
            dbg.error('Don\'t know what op to use for "{}"'.format(expr.parameter))

        # Reserve a temporary value
        if dest is None:
            dest = Value()
            
        # Create IR instruction
        block.code.append(Instr(op, [a, b], dest))

        return dest

    elif isinstance(expr, Token):
        if expr == Token('NAME'):
            value = Name(expr.text)

        elif expr == Token('INT'):
            value = Const(int(expr.text))
        
        else:
            dbg.error('Failed to process expression for {}'.format(expr))
            sys.exit(1)
        
        # If a destination is provided then we need to put it there
        if dest != None:
            block.code.append(Instr('COPY', [value,], dest))

        return value

    elif expr == Node('FUNCTION'):
        dbg.debug('Processing function')
        if expr.parameter == Node('COMMALIST'):
            argTokens = expr.parameter.components
        else:
            argTokens = [expr.parameter]
        
        # TODO: Ensure that all args are names, or is that already done?
        # No it is not already done, it crashes when a ParseFailed somehow lands in argTokens
        args = [Name(argToken.text) for argToken in argTokens]

        newBlock = Block(args)
        ir.append(newBlock)
        processBlock(expr.components[0], newBlock)

        # Reserve a temporary value
        if dest is None:
            dest = Value()
        block.code.append(Instr('FUNCTION', [newBlock], dest))

        return dest

    elif expr == Node('CALL'):
        # Reserve a temporary value
        if dest is None:
            dest = Value()
        
        # Create the call instruction
        processCall(expr, block, dest)

    else:
        dbg.error('Don\'t know what to do with this: {}'.format(expr))
        sys.exit(1)

def processCall(call, block, dest=None):
    # Unlike processExpression, here dest=None indicates that there is no destination. Create the
    # call instruction using the provided dest. If dest == None then Instr will handle it properly.

    dbg = debugHandler('irGen.processcall')
    dbg.debug('Processing call')

    target = Name(call.parameter)
    if call.components[0] == Node('COMMALIST'):
        dbg.debug('Args are commalist')
        args = [processExpression(comp, block) for comp in call.components[0].components]
    else:
        dbg.debug('Args is expression')
        args = [processExpression(call.components[0], block)]

    block.code.append(Instr('CALL', [target, *args], dest))

def processLoop(loop, block, dest=None):
    dbg = debugHandler('irGen.processLoop')
    dbg.debug('Processing loop')

    # 8 types of instruction can be generated here (it seems simpler to generate different
    # instructions than to allow args that drastically change the execution behavior). They are
    # combinations of the base LOOP instruction and T, I, and C. LOOP is just an infinite loop.
    # With T it is limited how many times it can loop. With I it iterates over an iterable. With C
    # it can only continue looping so long as the specified condition expression evaluates to true.
    # Possible combinations are: LOOP, LOOPT, LOOPI, LOOPC, LOOPTI, LOOPTC, LOOPIC, and LOOPTIC

    instrOp =  'LOOP'
    instrArgs = []

    timesParameter = loop.parameter['times']
    if timesParameter != None:
        instrOp = instrOp + 'T'
        
        if timesParameter == Token('INT'):
            instrArgs.append(Const(timesParameter.text))
        
        elif timesParameter == Token('NAME'):
            instrArgs.append(Name(timesParameter.text))
        
        else:
            dbg.error('Expected an int or name for times modifier, got {}'.format(timesParameter))
            sys.exit(1)

    iterateParameter = loop.parameter['iterate']
    if iterateParameter != None:
        instrOp = instrOp + 'I'
        dbg.warning('Iterate parameter not fully implemented')

        if iterateParameter[0] == Token('NAME'):
            instrArgs.append(Name(iterateParameter[0].text))

        else:
            dbg.error('Expected a name for iterator, got {}'.format(iterateParameter[0]))

        # TODO: Implement iterables and check that either a name or iterable is provided
        # This is a band-aid to mke sure code generation for iteration loops sort of works
        instrArgs.append(Name(iterateParameter[1].text))

    conditionParameter = loop.parameter['condition']
    if conditionParameter != None:
        instrOp = instrOp + 'C'

        dbg.warning('Condition parameter not supported')

    # Create a block in the IR in which to put the loop body
    # This block will never be called; the LOOP instruction will handle it
    dbg.debug('Creating IR block for loop body')
    loopBlock = Block([])
    ir.append(loopBlock)
    processBlock(loop.components[0], loopBlock)

    instrArgs.insert(0, loopBlock)

    block.code.append(Instr(instrOp, instrArgs, dest=dest))
