import copy, sys

import lexer
from lexer import Token
from debug import debugHandler


class ParserError:
    def __init__(self, line, col, message=''):
        self.line = line
        self.col = col
        self.message = message

    def __repr__(self):
        return '{} l{}c{}{}{}'.format(type(self).__name__, self.line, self.col, ': ' if self.message != '' else '', self.message)

# Provided stream is not what the parser wanted
class NoMatch(ParserError): pass

# Provided stream appeared to be one thing but was not correct
class ParseFailed(ParserError): pass


class Node:
    compareParameter = False
    reprComponents = False

    class tempCompareParameter():
        # There is no need to keep a stack of values because in the event that another context
        # manager is created it will independently track the value through out its indented block
        def __init__(self, value):
            self.value = value

        def __enter__(self):
            self.originalCompareParameter = Node.compareParameter
            Node.compareParameter = self.value

        def __exit__(self, excType, excValue, excTraceback):
            Node.compareParameter = self.originalCompareParameter

    def __init__(self, nodeType, line=None, col=None, parameter = None, components = []):
        self.nodeType = nodeType
        self.line = line
        self.col = col
        self.parameter = parameter
        self.components = components

    def __repr__(self):
        text = 'node {}'.format(self.nodeType)
        
        if not self.parameter is None:
            text = text + ' ({})'.format(self.parameter)
        
        if self.line != None and self.col != None:
            text = text + ' (l{}c{})'.format(self.line, self.col)

        if self.reprComponents:
            for component in self.components:
                for line in repr(component).splitlines():
                    text = text + '\n    {}'.format(line)

        return text

    def __eq__(self, other):
        if isinstance(other, Node):
            result = self.nodeType == other.nodeType
            if self.compareParameter:
                result = result and self.parameter == other.parameter
            return result
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def build(self, components):
        if self.nodeType == 'BLOCK':
            self.components = components

        elif self.nodeType == 'DIRECTIVE':
            assert len(components) >= 1
            self.parameter = components[0].tokenType[1:]
            self.components = components[1:]

        elif self.nodeType == 'STORE':
            assert len(components) == 2
            self.parameter = components[0]
            self.components = components[1:]

        elif self.nodeType == 'BREAK':
            pass    # This one's only here so you can call Node.build, primarily for consistency's sake

        elif self.nodeType == 'EXPRESSION':
            assert len(components) == 3
            self.parameter = components[1].text
            self.components = [components[0], components[2]]


        
        elif self.nodeType == 'CONSTANT':
            assert len(components) == 1
            self.parameter = components[0]

        elif self.nodeType == 'STORE':
            assert len(components) == 3
            self.parameter = components[0]
            self.components = [components[2]]

        elif self.nodeType == 'CALL':
            assert len(components) == 3
            self.parameter = components[0]
            self.components = []

        elif self.nodeType == 'COMMALIST':
            dbg.warning('Node type COMMALIST is not built')

        else:
            raise NotImplementedError('Cannot build node type {}'.format(self.nodeType))


def parseStream(stream, reportData):
    dbg = debugHandler('parser.parseStream')
    dbg.debug('Parsing stream')

    lastIndentLevel = 0
    blockStarts = []
    blockPositions = []

    # Identify blocks in the source
    for t, token in enumerate(stream):
        if token == Token('NEWLINE'):

            # Measure indentation
            indentLevel = 0
            i = 1
            try:
                while stream[t + i] == Token('INDENT'):
                    indentLevel += 1
                    i += 1
            except IndexError:
                # dbg.debug('NEWLINE at end of stream, forcing indent level 0')
                indentLevel = 0     # Newline at end of stream, treat as indent 0

            # dbg.debug('Indent level: {}'.format(indentLevel))

            # An increase in indentation indicates the start of a new block
            # Record t in blockStarts and adjust indentLevel
            if indentLevel > lastIndentLevel:
                # t points to newline before block, start the block at the first indent after
                blockStarts.append(t + 1)
                lastIndentLevel = indentLevel

            # A decrease in indentation indicates the end of one or more blocks
            # Record all blocks that were finished in blockPositions and clean up blockStarts
            while indentLevel < lastIndentLevel:
                blockPositions.append((blockStarts[-1], t))
                del blockStarts[-1]
                lastIndentLevel -= 1

    # dbg.debug('blockStarts final value: {}'.format(blockStarts))

    # If the end of stream is still indented, record the final blocks in blockPositions and
    # clean up blockStarts
    while lastIndentLevel > 0:
        blockPositions.append((blockStarts[-1], len(stream) - 1))
        del blockStarts[-1]
        lastIndentLevel -= 1

    dbg.debug('blockPositions: {}'.format(blockPositions))

    # Parse individual blocks in a bottom-up manner
    for b0, blockPosition in enumerate(blockPositions):
        blockStart, blockStop = blockPosition
        
        blockStream = stream[blockStart:blockStop + 1]  # blockStop points to the last item, need to index one 
        del stream[blockStart:blockStop + 1]            # Remove the block stream, including the newline at the end

        # Parse the block stream and insert it into the stream
        dbg.debug('Parsing block at {}'.format(blockStart))
        result = parseBlock(blockStream)
        if isinstance(result, ParseFailed):
            dbg.error('l{}c{}: Failed to parse block ({})'.format(blockStream[0].line, blockStream[0].col, result))
            sys.exit(1)
        stream.insert(blockStart, result)

        # stop - start = blockStream length - 1 = difference in stream length
        offset = blockStop - blockStart
        
        # Decrement blockPositions to account for change in stream length
        for b1, otherBlockPosition in enumerate(blockPositions[b0:]):
            otherBlockStart, otherBlockStop = otherBlockPosition

            if otherBlockStart >= blockStop: otherBlockStart -= offset
            if otherBlockStop >= blockStop: otherBlockStop -= offset

            blockPositions[b0 + b1] = (otherBlockStart, otherBlockStop)

        dbg.debug('Adjusted block positions: {}'.format(blockPositions))

    # Parse the stream as a block (0 indent)
    dbg.debug('Parsing stream as a block')
    topNode = parseBlock(stream)

    if isinstance(topNode, ParseFailed):
        dbg.error('Failed to parse block ({})'.format(topNode))
        # dbg.error('l{}c{}: Failed to parse block ({})'.format(topNode.line, topNode.col, topNode))
        sys.exit(1)

    Node.reprComponents = True
    dbg.debug('topNode:\n{}'.format(topNode))

    if reportData.generate:
        with open(reportData.ast, 'w') as reportFile:
            reportFile.write(repr(topNode) + '\n')

    return topNode


def parseBlock(rawStream):
    dbg = debugHandler('parser.parseBlock')
    dbg.debug('Attempting to parse block')
    stream = rawStream
    currentItemStream = []
    itemStreams = []
    items = []

    dbg.debug('First token is {}'.format(stream[0]))
    
    # Separate stream into itemStreams
    for token in stream:
        if not token in (Token('NEWLINE'), Node('BLOCK')):
            currentItemStream.append(token)
        else:
            # Blocks that aren't the main block in the file may be preceeded with a newline, which
            # results in an empty itemStream
            if currentItemStream != []:
                itemStreams.append(currentItemStream)
                currentItemStream = []

            if token == Node('BLOCK'):
                itemStreams.append([token])

    # Catch whatever might by left due to no trailing newline
    if currentItemStream != []:
        itemStreams.append(currentItemStream)

    # Strip block indentation
    for itemStream in itemStreams:
        while itemStream[0] == Token('INDENT'):
            del itemStream[0]

    # Tack item streams that are just blocks onto the previous item stream
    i = 0
    while True:
        if i >= len(itemStreams): break
        itemStream = itemStreams[i]
        dbg.debug(itemStream)

        if i >= 1 and itemStream == [Node('BLOCK')]:
            dbg.debug('Tacking block onto item stream before it')
            # TODO: Get correct line and col for this newline
            itemStreams[i - 1].append(itemStream[0])    #  += [Token('NEWLINE', text='\n'), itemStream[0]]
            del itemStreams[i]

        else:
            i += 1

    for i, itemStream in enumerate(itemStreams):
        dbg.debug('Parsing itemStream:\n{}'.format(itemStream))

        # Blocks can contain other blocks that were parsed already
        if isinstance(itemStream[0], Node):
            if itemStream[0] == Node('BLOCK'):
                dbg.debug('Encountered block, passing it on')
                items.append(itemStream[0]); continue
            dbg.error('l{}c{}: Encountered a node that wasn\'t a block, don\'t know what to do'.format(itemStream[0].line, itemStream[0].col))
            sys.exit(1)
        
        result = parseDirective(itemStream)
        if isinstance(result, ParseFailed): return result
        elif isinstance(result, Node): items.append(result); continue
        elif not isinstance(result, NoMatch):
            dbg.error('l{}c{}: parseDirective returned unexpected value ({})'.format(result.line, result.col, result))
            sys.exit(1)
        directiveResult = result

        result = parseStatement(itemStream)
        if isinstance(result, ParseFailed): return result
        elif isinstance(result, Node): items.append(result); continue
        elif not isinstance(result, NoMatch):
            dbg.error('l{}c{}: parseStatement returned unexpected value ({})'.format(result.line, result.col, result))
            sys.exit(1)
        statementResult = result
        
        # dbg.error('parseBlock failed ({})'.format(itemStream))
        return ParseFailed(itemStream[0].line, itemStream[0].col, 'Statement and directive both failed to match ({} | {})'.format(directiveResult, statementResult))

    node = Node('BLOCK', line=stream[0].line, col=itemStreams[0][0].col)
    node.build(items)

    return node


def parseDirective(stream):
    dbg = debugHandler('parser.parseDirective')
    dbg.debug('Attempting to parse directive')
    if stream[0] != Token('DIRECTIVE'):
        return NoMatch(stream[0].line, stream[0].col, 'Not a directive')

    node = Node('DIRECTIVE', line=stream[0].line, col=stream[0].col)
    node.build(stream)
    
    return node


def parseStatement(stream):
    dbg = debugHandler('parser.parseStatement')
    dbg.debug('Attempting to parse statement')

    result = parseStore(stream)
    if not isinstance(result, NoMatch): return result

    result = parseCall(stream)
    if not isinstance(result, NoMatch): return result

    result = parseReturn(stream)
    if not isinstance(result, NoMatch): return result

    result = parseLoop(stream)
    if not isinstance(result, NoMatch): return result

    result = parseBreak(stream)
    if not isinstance(result, NoMatch): return result

    result = parseIf(stream)
    if not isinstance(result, NoMatch): return result

    result = parseElsif(stream)
    if not isinstance(result, NoMatch): return result

    result = parseElse(stream)
    if not isinstance(result, NoMatch): return result

    return NoMatch(stream[0].line, stream[0].col, 'Not a valid statement')


def parseStore(stream):
    if stream[0:2] != [Token('NAME'), Token('EQUALS')]:
        return NoMatch(stream[0].line, stream[0].col, 'Not a store')

    value = None

    expr = parseExpression(stream[2:])
    if not isinstance(expr, NoMatch):
        value = expr

    if value is None:
        loop = parseLoop(stream[2:])
        if not isinstance(loop, NoMatch):
            value = loop

    if value is None:
        return ParseFailed(stream[0].line, stream[0].col, 'parseExpression and parseLoop failed to match ({} | {})'.format(expr, loop))
    elif isinstance(value, ParseFailed):
        return ParseFailed(stream[0].line, stream[0].col, 'Failed to parse value ({})'.format(value))

    node = Node('STORE', line=stream[0].line, col=stream[0].col)
    node.build([stream[0].text, value])

    return node


def parseExpression(stream):
    dbg = debugHandler('parser.parseExpression')
    exprStream = copy.copy(stream)  # Avoid damaging the original stream

    dbg.debug('Attempting to parse expression:\n{}'.format(exprStream))

    if stream[0] == Token('KEYWORD'):
        dbg.debug('Attempting to parse keyword expression')
        if stream[0].text == 'function':
            result = parseFunction(stream)
            if isinstance(result, NoMatch):
                return ParseFailed(stream[0].line, stream[0].col, 'parseFunction failed to match ({})'.format(result))
            else: return result

        else:
            return ParseFailed(stream[0].line, stream[0].col, 'Keyword {} is not valid to begin an expression'.format(stream[0].text))

    elif Token('LPAREN') in stream or Token('RPAREN') in stream:
        dbg.debug('Attempting to parse parenthetic expression')
        index = 0
        starts = []
        depth = 0

        while index < len(exprStream):
            token = exprStream[index]
            if token == Token('LPAREN'):
                depth += 1
                starts.append(index)

            elif token == Token('RPAREN'):
                depth -= 1

                if depth < 0:
                    return ParseFailed(stream[0].line, stream[0].col, 'RPAREN with no LPAREN')

                start = starts[-1]
                del starts[-1]

                if start > 0 and exprStream[start - 1] == Token('NAME'):
                    dbg.debug('Parenthetic expression is actually call')
                    # Parse the call
                    start -= 1
                    expr = parseCall(exprStream[start:index + 1])
                else:
                    # Parse the expression contained within the parentheses
                    expr = parseExpression(exprStream[start + 1:index])
                
                del exprStream[start:index + 1]
                exprStream.insert(start, expr)

                index = start

            index += 1

        if depth > 0:
            return ParseFailed(stream[0].line, stream[0].col, 'LPAREN with no RPAREN')

        # If the entire expression isn't contained within parentheses, the parenthetic section
        # won't parse it all and there will be more than one item in exprStream
        if len(exprStream) == 1:
            return expr
        else:
            return parseExpression(exprStream)

    elif stream.count(Token('OPERATOR')) > 1:
        dbg.debug('Attempting to parse complex expression')

        operatorPrecedence = [
            ['**'],
            ['*', '/', '%'],
            ['+', '-'],
            ['<', '>', '<=', '>=', '==']
        ]
        # Not used yet
        operatorConsumption = {
            '**': (-1, 3),
            '*': (-1, 3),
            '/': (-1, 3),
            '%': (-1, 3),
            '+': (-1, 3),
            '-': (-1, 3),
            '<': (-1, 3),
            '>': (-1, 3),
            '<=': (-1, 3),
            '>=': (-1, 3),
            '==': (-1, 3)
        }

        with Token.tempCompareText(True):
            for precedenceGroup in operatorPrecedence:
                for operator in precedenceGroup:
                    # dbg.debug('Checking operator {}'.format(operator))
                    compareToken = Token('OPERATOR', text=operator)
                    try:
                        while True:
                            index = exprStream.index(compareToken)
                            # dbg.debug('Found operator at {}'.format(index))

                            expr = parseExpression(exprStream[index - 1:index + 2])
                            del exprStream[index - 1:index + 2]

                            exprStream.insert(index - 1, expr)

                    except ValueError:
                        continue

        return exprStream[0]

    # elif stream.count(Token('OPERATOR')) == 1:
    elif len(stream) == 3:
        # TODO: Check to ensure that stream[1] is token OPERATOR
        dbg.debug('Attempting to parse simple expression')

        node = Node('EXPRESSION', line=exprStream[0].line, col=exprStream[0].col)
        node.build(exprStream)

        return node

    else:
        dbg.debug('Attempting to parse expression component')

        if len(exprStream) == 1 and exprStream[0] in [Token('NAME'), Token('INT'), Token('FLOAT'), Token('STRING')]:
            return exprStream[0]

        else:
            dbg.debug('{} is not an expression'.format(exprStream))
            return NoMatch(stream[0].line, stream[0].col, 'Not an expression')


def parseCall(stream):
    if stream[0] != Token('NAME') or stream[1] != Token('LPAREN') or stream[-1] != Token('RPAREN'):
        return NoMatch(stream[0].line, stream[0].col, 'Not a call')

    node = Node('CALL', line=stream[0].line, col=stream[0].col, parameter=stream[0].text)

    # Arguments provided
    if len(stream) > 3:
        if stream.count(Token('COMMA')):
            result = parseCommaList(stream[2:-1])
        else:
            result = parseExpression(stream[2:-1])

        if isinstance(result, NoMatch): return ParseFailed(stream[0].line, stream[0].col, 'Invalid arguments')
        if isinstance(result, ParseFailed): return ParseFailed(stream[0].line, stream[0].col, 'Invalid arguments ({})'.format(result.message))

        node.components = [result]

    return node


def parseCommaList(stream):
    items = [[]]
    for item in stream:
        if item == Token('COMMA'):
            items.append([])
        else:
            items[-1].append(item)

    node = Node('COMMALIST', line=stream[0].line, col=stream[0].col)
    node.components = [parseExpression(item) for item in items]
    return node


def parseFunction(stream):
    dbg = debugHandler('parser.parseFunction')
    dbg.debug('Attempting to parse function')
    with Token.tempCompareText(True):
        if stream[0] != Token('KEYWORD', text='function') \
            or stream[-2] != Token('COLON', text=':') \
            or stream[-1] != Node('BLOCK'):
            return NoMatch(stream[0].line, stream[0].col, 'Not a function')
    
    argStream = stream[1:-2]
    if len(argStream) == 1:
        args = argStream[0]
    else:
        result = parseCommaList(argStream)
        if isinstance(result, NoMatch):
            return ParseFailed(stream[0].line, stream[0].col, 'Malformed args ({})'.format(result))
        args = result

    # The block following the function is not connected to the function node at this time. After
    # parseBlock returns the main block, parseSource walks the ast and connects functions to their
    # blocks.

    node = Node('FUNCTION', line=stream[0].line, col=stream[0].col)
    node.parameter = args
    node.components = [stream[-1]]

    return node


def parseReturn(stream):
    dbg = debugHandler('parser.parseReturn')
    dbg.debug('Attempting to parse return')

    with Token.tempCompareText(True):
        if stream[0] != Token('KEYWORD', text='return'):
            return NoMatch(stream[0].line, stream[0].col, 'Not a return')

    node = Node('RETURN', line=stream[0].line, col=stream[0].col)
    if len(stream) > 1:

        result = parseExpression(stream[1:])
        if isinstance(result, ParseFailed):
            return ParseFailed(stream[0].line, stream[0].col, 'Invalid return value ({})'.format(result))
        elif not isinstance(result, NoMatch):
            node.components = [result]; return node

        result = parseCommaList(stream[1:])
        if isinstance(result, ParseFailed):
            return ParseFailed(stream[0].line, stream[0].col, 'Invalid return value ({})'.format(result))
        elif not isinstance(result, NoMatch):
            node.components = [result]; return node

    else:
        return node


# Find a Token in a given stream, return the index
def _findToken(stream, searchToken):
    for t, token in enumerate(stream):
        if token == searchToken:
            return t

def parseLoop(stream):
    dbg = debugHandler('parser.parseLoop')
    dbg.debug('Attempting to parse loop')
    with Token.tempCompareText(True):
        if stream[0] != Token('KEYWORD', text='loop') \
            or stream[-2] != Token('COLON', text=':') \
            or stream[-1] != Node('BLOCK'):
            return NoMatch(stream[0].line, stream[0].col, 'Not a loop')

    dbg.warning('\'parseLoop\' not fully implemented')

    modifiersStream = stream[1:-2]
    
    dbg.debug('Checking for times modifier ({})'.format(modifiersStream))
    timesModifier = None

    if len(modifiersStream) >= 1:
        if modifiersStream[0] in (Token('INT'), Token('NAME')):
            dbg.debug('Parsing times modifier')
            timesModifier = modifiersStream[0]
            del modifiersStream[0]
        # else:
        #     return ParseFailed('Malformed loop modifiers ({})'.format(modifiersStream))

    dbg.debug('Checking for iterate modifier ({})'.format(modifiersStream))
    iterateModifier = None
    # This needs to be set now in case the iterateModifier isn't checked
    isConditionModifier = False

    if len(modifiersStream) >= 4:
        isIterateModifier = False
        with Token.tempCompareText(True):
            if modifiersStream[0] == Token('KEYWORD', text='for'):
                dbg.debug('Parsing iterate modifier')

                # Search for `in` keyword
                inkwIndex = _findToken(modifiersStream, Token('KEYWORD', text='in'))
                if inkwIndex is None:
                    return ParseFailed(stream[0].line, stream[0].col, 'Iterate modifier missing `in` keyword ({})'.format(modifiersStream))
                isIterateModifier = True

                # Search for `while` keyword
                whilekwIndex = _findToken(modifiersStream, Token('KEYWORD', text='while'))
                isConditionModifier = not whilekwIndex is None

            # TODO: Search for `:` or `while` to indicate end of iterable

        if isIterateModifier:
            iteratorStream = modifiersStream[1:inkwIndex]
            if isConditionModifier:
                iterableStream = modifiersStream[inkwIndex + 1:whilekwIndex]
            else:
                iterableStream = modifiersStream[inkwIndex + 1:]
                
            # dbg.debug('iIn: {}'.format(inkwIndex))
            # dbg.debug('iteratorStream: {}'.format(iteratorStream))
            # dbg.debug('iterableStream: {}'.format(iterableStream))
            
            if len(iteratorStream) < 1:
                return ParseFailed(stream[0].line, stream[0].col, 'Missing iterator')
            if len(iteratorStream) > 1:
                return ParseFailed(stream[0].line, stream[0].col, 'Too many tokens for iterator ({})'.format(iteratorStream))
            
            iterator = iteratorStream[0]
            
            if iterator != Token('NAME'):
                return ParseFailed(stream[0].line, stream[0].col, 'Expected {} for iterator, got {}'.format(Token('NAME'), iterator))

            iterable = parseExpression(iterableStream)

            if isinstance(iterable, ParserError):
                return ParseFailed(stream[0].line, stream[0].col, 'Iterable failed to parse ({})'.format(iterable))

            iterateModifier = (iterator, iterable)
            if isConditionModifier:
                del modifiersStream[0:whilekwIndex]
            else:
                # Even if there's no other modifiers to check, delete what was consumed, otherwise
                # it will get flagged as excess code at the end of `parseLoop`
                del modifiersStream[0:]

    dbg.debug('Checking for condition modifier ({})'.format(modifiersStream))
    conditionModifier = None

    # The iterate modifier has to check for the `while` keyword. If it does find it,
    # `ifConditionModifier` will True/False, otherwise None.
    if isConditionModifier is None:
        with Token.tempCompareText():
            isConditionModifier = modifiersStream[0] == Token('KEYWORD', text='while')

    if isConditionModifier:
        dbg.debug('Parsing condition modifier')
        if len(modifiersStream) == 1:   # Only the `while` keyword present
            return ParseFailed(stream[0].line, stream[0].col, 'while keyword with no expression')
        
        expr = parseExpression(modifiersStream[1:])
        if isinstance(expr, ParserError):
            return ParseFailed(stream[0].line, stream[0].col, 'Failed to parse condition expression ({})'.format(expr))
        conditionModifier = expr
        del modifiersStream[0:]

    # Check for leftover tokens after parsing all modifiers
    if len(modifiersStream):
        return ParseFailed(stream[0].line, stream[0].col, 'Excess tokens in modifiersStream: {}'.format(modifiersStream))

    node = Node('LOOP', line=stream[0].line, col=stream[0].col)
    node.parameter = {
        'times': timesModifier,
        'iterate': iterateModifier,
        'condition': conditionModifier
    }
    node.components = [stream[-1]]

    return node


def parseBreak(stream):
    with Token.tempCompareText(True):
        if stream != [Token('KEYWORD', text='break')]:
            return NoMatch(stream[0].line, stream[0].col, 'Not a break')

    node = Node('BREAK', line=stream[0].line, col=stream[0].col)
    node.build([])

    return node


def parseIf(stream):
    with Token.tempCompareText(True):
        if stream[0] != Token('KEYWORD', text='if') \
            or stream[-2] != Token('COLON', text=':') \
            or stream[-1] != Node('BLOCK'):
            return NoMatch(stream[0].line, stream[0].col, 'Not an if statement')
    
    conditionExpr = parseExpression(stream[1:-2])
    if type(conditionExpr) in (NoMatch, ParseFailed):
        return ParseFailed(stream[1].line, stream[1].col, 'Expected an expression ({})'.format(conditionExpr))

    return Node(
        'IF',
        line=stream[0].line,
        col=stream[0].col,
        parameter=conditionExpr,
        components=[stream[-1]]
    )

def parseElsif(stream):
    with Token.tempCompareText(True):
        if stream[0] != Token('KEYWORD', text='elsif') \
            or stream[-2] != Token('COLON', text=':') \
            or stream[-1] != Node('BLOCK'):
            return NoMatch(stream[0].line, stream[0].col, 'Not an elsif statement')

    conditionExpr = parseExpression(stream[1:-2])
    if type(conditionExpr) in (NoMatch, ParseFailed):
        return ParseFailed(stream[1].line, stream[1].col, 'Expected an expression ({})'.format(conditionExpr))

    return Node(
        'ELSIF',
        line=stream[0].line,
        col=stream[0].col,
        parameter=conditionExpr,
        components=[stream[-1]]
    )

def parseElse(stream):
    with Token.tempCompareText(True):
        if stream[0] != Token('KEYWORD', text='else') \
            or stream[1] != Token('COLON', text=':') \
            or stream[2] != Node('BLOCK') \
            or len(stream) != 3:
            return NoMatch(stream[0].line, stream[0].col, 'Not an else statement')

    return Node(
        'ELSE',
        line=stream[0].line,
        col=stream[0].col,
        components=[stream[-1]]
    )
