# General ideas

* Python-like syntax, not visually noisy
* Dynamic typing, program is walked at compilation to guess types beforehand
* Compiler directives available to tweak behavior, like `@namespace inhibit modify, create`
* Object-oriented for certain
* Loop types: for/in, while
* All loops can have broken and unbroken sections defined that run when the loop breaks or at the end if it doesn't break
* Runtime error catching with try/fail/succeed


# Loops

Loops begin with the `loop` keyword and are followed by a code block.

## Modifiers

You can add any combination of the following modifiers (in order) to a loop statement:

* `<int> times` - Can be a name or literal, provides a set iteration count
* `for <name> in <iterable>` - The loop iterates over each item in the given iterable and provides each one as the given name
* `while <condition>` - The loop will test the condition at the start of each iteration and exit if the condition is false

If you store the loop in a name (`<name> = loop <modifiers>:`) then the current iteration will be stored in that name as an int.

## Examples

```
loop:
loop <name>:
loop <int> times:
loop <name> <int> times:
loop for <name> in <array_like>:
loop while <condition>:
```

# Compiler steps:

1. Lexical analysis
2. Lexical stripping
3. Parsing to AST
4. IR generation
5. Backend

# Roadmap

1. ~~Parse function calls~~
2. ~~Separate program output from debugging stream~~
3. ~~Compile AST to IR~~
4. ~~Set up interpreter backend~~ replaced with pyscript backend
5. Parse loops
6. Set up multiple data types
    - int
    - str
    - float
7. Parse if/else
